package ru.ottercoder.test1.model;

import javax.persistence.Entity;
import java.util.UUID;

@Entity
public class Log {
    private String uid;
    private String phoneNumber;
    private int status;

    public Log(String phoneNumber, int status) {
        this.uid = UUID.randomUUID().toString();
        this.phoneNumber = phoneNumber;
        this.status = status;
    }

    public String getUid() {
        return uid;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public int getStatus() {
        return status;
    }
}
