package ru.ottercoder.test1.model;

public class PhoneNumberInput {

    private String phoneNumber;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public String toString() {
        return "PhoneNumberInput{" +
                "phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}
