package ru.ottercoder.test1.model;

public enum ActionStatus {

    SUCCESS(1),
    FAIL(2);

    private int statusId;

    ActionStatus(int statusId) {
        this.statusId = statusId;
    }

    public int getStatusId() {
        return statusId;
    }
}
