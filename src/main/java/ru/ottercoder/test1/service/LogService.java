package ru.ottercoder.test1.service;

import org.springframework.http.HttpStatus;
import ru.ottercoder.test1.model.PhoneNumberInput;

import java.util.Map;

public interface LogService {

    void logAction(HttpStatus status, PhoneNumberInput phoneNumberInput);

    Map<String, Integer> getLogStats();

}
