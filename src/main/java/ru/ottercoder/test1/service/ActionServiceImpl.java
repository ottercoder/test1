package ru.ottercoder.test1.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ru.ottercoder.test1.model.PhoneNumberInput;

@Service
public class ActionServiceImpl implements ActionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ActionServiceImpl.class);

    private final ExternalService externalService;
    private final LogService logService;

    @Autowired
    public ActionServiceImpl(ExternalService externalService, LogService logService) {
        this.externalService = externalService;
        this.logService = logService;
    }

    @Override
    public void addAction(PhoneNumberInput phoneNumberInput) {
        LOGGER.info("Starting external request for phoneNumber: {}", phoneNumberInput.getPhoneNumber());
        ResponseEntity<String> responseEntity = externalService.doRequest(phoneNumberInput);

        LOGGER.info("Logging response of external service is: {}", responseEntity);
        logService.logAction(responseEntity.getStatusCode(), phoneNumberInput);
    }


}
