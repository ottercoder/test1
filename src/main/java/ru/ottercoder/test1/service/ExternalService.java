package ru.ottercoder.test1.service;

import org.springframework.http.ResponseEntity;
import ru.ottercoder.test1.model.PhoneNumberInput;

public interface ExternalService {

    ResponseEntity<String> doRequest(PhoneNumberInput phoneNumberInput);
}
