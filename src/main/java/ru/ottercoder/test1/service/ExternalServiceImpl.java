package ru.ottercoder.test1.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.ottercoder.test1.exception.JsonProcessingFromInputException;
import ru.ottercoder.test1.model.PhoneNumberInput;

@Service
public class ExternalServiceImpl implements ExternalService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExternalServiceImpl.class);

    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;

    @Value("${external_service.url}")
    private String externalServiceUrl;


    @Autowired
    public ExternalServiceImpl(RestTemplate restTemplate, ObjectMapper objectMapper) {
        this.restTemplate = restTemplate;
        this.objectMapper = objectMapper;
    }

    @Override
    public ResponseEntity<String> doRequest(PhoneNumberInput phoneNumberInput) {
        try {
            LOGGER.info("Creating json from object = {}", phoneNumberInput);
            HttpHeaders headers = new HttpHeaders();
            String json = getJsonFromPhoneNumberInput(phoneNumberInput);
            headers.set("Accept", "application/json");
            HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);
            LOGGER.info("Starting request to external url with json: {}", json);
            return restTemplate.exchange(externalServiceUrl, HttpMethod.GET, requestEntity, String.class);
        } catch (Exception e) {
            LOGGER.error("Error while doing external request with json: {}", externalServiceUrl, e);
            throw e;
        }
    }

    private String getJsonFromPhoneNumberInput(PhoneNumberInput phoneNumberInput) {
        try {
            return objectMapper.writeValueAsString(phoneNumberInput);
        } catch (JsonProcessingException e1) {
            LOGGER.error("Can't create json from: {}", phoneNumberInput);
            throw new JsonProcessingFromInputException();
        }
    }

}
