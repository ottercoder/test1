package ru.ottercoder.test1.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ottercoder.test1.dao.LogDao;
import ru.ottercoder.test1.model.PhoneNumberInput;

import java.util.Map;

@Service
public class LogServiceImpl implements LogService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogServiceImpl.class);

    private final LogDao logDao;

    @Autowired
    public LogServiceImpl(LogDao logDao) {
        this.logDao = logDao;
    }

    @Override
    @Transactional
    public void logAction(HttpStatus status, PhoneNumberInput phoneNumberInput) {
        LOGGER.info("Status of external request for phoneNumberInput: {} is {}", phoneNumberInput, status.value());
        if (status.is2xxSuccessful()) {
            incrementCounter(phoneNumberInput.getPhoneNumber());
            incrementCounter("successSum");
        }
        incrementCounter("wholeSum");
    }

    @Override
    public Map<String, Integer> getLogStats() {
        return logDao.getCounters();
    }

    private void incrementCounter(String counterName) {
        LOGGER.info("Incrementing counter: {}", counterName);
        logDao.incrementCounter(counterName);
    }
}