package ru.ottercoder.test1.exception;


public class JsonProcessingFromInputException extends RuntimeException {

    public JsonProcessingFromInputException() {
        super("Can't process json from input");
    }
}
