package ru.ottercoder.test1.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class LogDaoImpl implements LogDao {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public LogDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    @Override
    public void incrementCounter(String counterName) {
        String sql = "update counters set counter_value = counter_value + 1 where counter_name = :counterName";
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("counterName", counterName);
        int result = jdbcTemplate.update(sql, params);
        if (result == 0) {
            insertCounter(counterName);
        }
    }

    @Override
    public Map<String, Integer> getCounters() {
        String sql = "select * from counters";
        return jdbcTemplate.query(sql, (ResultSetExtractor<Map<String, Integer>>) rs -> {
            HashMap<String, Integer> mapRet = new HashMap<>();
            while (rs.next()) {
                mapRet.put(rs.getString("counter_name"), rs.getInt("counter_value"));
            }
            return mapRet;
        });
    }

    private void insertCounter(String counterName) {
        String sql = "insert into counters (counter_name, counter_value) values (:counterName, 1)";
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("counterName", counterName);
        jdbcTemplate.update(sql, params);
    }
}