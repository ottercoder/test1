package ru.ottercoder.test1.dao;

import java.util.Map;

public interface LogDao {

    void incrementCounter(String counterName);

    Map<String, Integer> getCounters();

}
