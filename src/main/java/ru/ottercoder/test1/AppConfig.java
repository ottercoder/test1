package ru.ottercoder.test1;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.SocketConfig;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Configuration
public class AppConfig {

    @Autowired
    Environment env;

    @Value("${external_service.url}")
    private String externalServiceUrl;
    @Value("${external_service.pool_size}")
    private int poolSize;
    @Value("${external_service.connection_timeout}")
    private int connectionTimeout;
    @Value("${external_service.read_timeout}")
    private int readTimeout;

    @Bean
    public RestTemplate externalServiceRestTemplate() {
        return new RestTemplate(createHttpClientRequestFactory(
                externalServiceUrl,
                poolSize,
                connectionTimeout,
                readTimeout));
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    private ClientHttpRequestFactory createHttpClientRequestFactory(String serviceUrl, Integer poolSize, Integer connectTimeout, Integer readTimeout) {
        return new HttpComponentsClientHttpRequestFactory(
                HttpClients.custom()
                        .setConnectionManager(createHttpConnectionManager(serviceUrl, poolSize))
                        .setDefaultRequestConfig(
                                RequestConfig.custom()
                                        .setConnectionRequestTimeout(connectTimeout)
                                        .setConnectTimeout(connectTimeout)
                                        .setSocketTimeout(readTimeout)
                                        .build()
                        )
                        .setDefaultSocketConfig(
                                SocketConfig.custom()
                                        .setSoTimeout(readTimeout)
                                        .build()
                        )
                        .build()
        );
    }

    private HttpClientConnectionManager createHttpConnectionManager(String serviceUrl, Integer poolSize) {

        PoolingHttpClientConnectionManager cp = new PoolingHttpClientConnectionManager();
        cp.setMaxTotal(poolSize);

        try {
            URI uri = new URI(serviceUrl);
            cp.setMaxPerRoute(new HttpRoute(new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme())), poolSize);
        } catch (Exception e) {
            cp.setDefaultMaxPerRoute(poolSize);
        }
        return cp;
    }
}
