package ru.ottercoder.test1.health;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;

@Component
public class ExternalServiceHealthIndicator implements HealthIndicator {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExternalServiceHealthIndicator.class);
    @Value("${external_service.url}")
    private URL url;
    @Value("${external_service.read_timeout}")
    private int timeout;

    @Override
    public Health health() {
        int errorCode = check();
        if (errorCode != 0) {
            return Health.down()
                    .withDetail("Error Code", errorCode).build();
        }
        return Health.up().build();
    }

    private int check() {
        boolean reachable = pingHost(url.getHost(),
                url.getPort() != -1 ? url.getPort() : url.getDefaultPort(),
                timeout);
        if (reachable) {
            return 0;
        } else {
            return 1;
        }
    }

    private static boolean pingHost(String host, int port, int timeout) {
        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress(host, port), timeout);
            return true;
        } catch (Exception e) {
            LOGGER.warn("Ping to host " + host + " has failed", e);
            return false;
        }
    }
}
