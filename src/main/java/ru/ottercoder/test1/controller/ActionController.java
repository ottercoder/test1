package ru.ottercoder.test1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.ottercoder.test1.model.PhoneNumberInput;
import ru.ottercoder.test1.service.ActionService;
import ru.ottercoder.test1.service.LogService;

import java.util.Map;

@RestController
@RequestMapping(path = {""}, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class ActionController {

    private final ActionService actionService;
    private final LogService logService;

    @Autowired
    public ActionController(ActionService actionService, LogService logService) {
        this.actionService = actionService;
        this.logService = logService;
    }

    @PostMapping("/action")
    public void addAction(@RequestBody PhoneNumberInput phoneNumberInput) {
        actionService.addAction(phoneNumberInput);
    }

    @GetMapping("/stat")
    public Map<String, Integer> getStat() {
        return logService.getLogStats();
    }
}
